import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateIdUtil } from '@ngcore/core';
import { LazyLoaderService } from '../../services/lazy-loader-service';


/**
 * Despite the name, this component is not actually "lazy".
 * It simply load the image in delayed/asynch manner..
 */
@Component({
  selector: 'common-lazy-image',
  template: `
  <img *ngIf="hasBothDimensions" [src]="htmlImageUrl" [alt]="altText" [width]="imgWidth" [height]="imgHeight">
  <img *ngIf="hasOnlyWidth" [src]="htmlImageUrl" [alt]="altText" [width]="imgWidth">
  <img *ngIf="hasOnlyHeight" [src]="htmlImageUrl" [alt]="altText" [height]="imgHeight">
  <img *ngIf="hasNeitherDimension" [src]="htmlImageUrl" [alt]="altText">
`
})
export class CommonLazyImageComponent {
  // @Input("iid") itemId: string;
  @Input("image-url") targetImageUrl: string;   // this image will be delay-loaded.
  @Input("alt-text") altText: string = '';
  @Input("width") imgWidth: number = 0;
  @Input("height") imgHeight: number = 0;
  @Input("delay") delay: (number | number[]) = 0;

  // <img> "src" attribute value.
  public htmlImageUrl: string;
  
  constructor(
    private lazyLoaderService: LazyLoaderService
  ) {
    // if(isDL()) dl.log('Hello CommonLazyImage Component. id = ' + this.navCtrl.id);

    this.htmlImageUrl = '';
  }

  ngOnInit() {
    // TBD:
    // Implementing the delaying loading logic...
    if (this.targetImageUrl) {  // Obviously, this should always be true..
      this.lazyLoaderService.getStringDelayed(this.targetImageUrl, this.delay).subscribe(url => {
        if(url) {
          this.htmlImageUrl = url;
        }
      });
    } else {
      this.htmlImageUrl = '';  // ???
    }
  }


  get hasBothDimensions(): boolean {
    return (this.imgWidth > 0 && this.imgHeight > 0);
  }
  get hasOnlyWidth(): boolean {
    return (this.imgWidth > 0 && this.imgHeight <= 0);
  }
  get hasOnlyHeight(): boolean {
    return (this.imgWidth <= 0 && this.imgHeight > 0);
  }
  get hasNeitherDimension(): boolean {
    return (this.imgWidth <= 0 && this.imgHeight <= 0);
  }


  setImageUrl(_url: string) {
    this.targetImageUrl = _url;
  }

}

import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
// import { NavController, AlertController, ToastController, Events } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
// import * as commonmark from 'commonmark';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateIdUtil } from '@ngcore/core';
import { LazyLoaderUtil } from '../../common/util/lazy-loader-util';


// tbd
// Placeholder, for now...

@Component({
  selector: 'lazy-loader-entry',
  template: `
  <div [innerHTML]="lazyText">
  </div>
`
})
export class LazyLoaderEntryComponent {

  lazyText: string;

  // @Input("iid") itemId: string;

  constructor(
    private httpClient: HttpClient,
  ) {
    // if(isDL()) dl.log('Hello LazyLoaderEntry Component. id = ' + this.navCtrl.id);

    // testing
    // this.lazyText = "<em>Hello</em>";
    this.lazyText = "";
  }
  ngOnInit() {
    // // variables can be read here....
    // // if(isDL()) dl.log(">>>>>> this.itemId = " + this.itemId);
    // if(isDL()) dl.log(">>>>>> this.markdownInput = " + this.markdownInput);
    // ....

  }

}

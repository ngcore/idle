import { NgModule, ModuleWithProviders, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
// import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { NgCoreCoreModule } from '@ngcore/core';
import { NgCoreBaseModule } from '@ngcore/base';

import { LazyLoaderService } from './services/lazy-loader-service';
import { LazyLoaderEntryComponent } from './components/lazy-loader-entry/lazy-loader-entry';
import { LazyLoaderButtonComponent } from './components/lazy-loader-button/lazy-loader-button';
import { CommonLazyImageComponent } from './components/common-lazy-image/common-lazy-image';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    // BrowserModule,
    // IonicModule,

    NgCoreCoreModule.forRoot(),
    NgCoreBaseModule.forRoot()
  ],
  declarations: [
    LazyLoaderEntryComponent,
    LazyLoaderButtonComponent,
    CommonLazyImageComponent
  ],
  exports: [
    LazyLoaderEntryComponent,
    LazyLoaderButtonComponent,
    CommonLazyImageComponent
  ],
  entryComponents: [
    LazyLoaderEntryComponent,
    // ????
  ]
})
export class NgCoreIdleModule {
  static forRoot(): ModuleWithProviders<NgCoreIdleModule> {
    return {
      ngModule: NgCoreIdleModule,
      providers: [
        // { provide: ErrorHandler, useClass: IonicErrorHandler },
        LazyLoaderService
      ]
    };
  }
}

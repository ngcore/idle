
import {throwError as observableThrowError, timer as observableTimer,  Observable } from 'rxjs';

import {mergeMap, concat, take, map, share, delay, retryWhen} from 'rxjs/operators';
import { Injectable, Inject, Optional } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClient } from '@angular/common/http';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
// import { DateTimeUtil, DateIdUtil } from '@ngcore/core';
import { UrlUtil } from '@ngcore/core';


/**
 * Delay loading service.
 */
@Injectable({
  providedIn : 'root'
})
export class LazyLoaderService {

  // tbd..
  // "Lazy loading" not implemented yet.
  // just testing....


  // Cache
  private textCache: { [url: string]: string } = {};
  private jsonCache: { [url: string]: Object } = {};

  constructor(
    @Optional() @Inject(APP_BASE_HREF) private origin: string,  // Needed for SSR
    private http: HttpClient
  ) {
    // if(isDL()) dl.log('Hello LazyLoaderService Provider');
  }


  /**
   * This does not actually do anthing.
   * It merely return the input text after the specified delay.
   */
  public getStringDelayed(text: string, delay: (number | number[])): Observable<string> {
    let millis = this.parseDelayMillis(delay);
    let obs = observableTimer(millis).pipe(map(i => {
      return text;
    }),share(),);
    return obs;
  }

  /**
   * Returns a delay time as specified, or a random time if a range is specified.
   *
   * @param delay Delay time millis or pair of start and end times in millis.
   */
  public parseDelayMillis(delay: (number | number[])): number {
    // if(isDL()) dl.log(`>> parseDelayMillis(): Input delay = ${delay}.`);
    // tbd:
    let millis = 0;
    if (typeof delay === 'number') {
      if (delay > 0) {
        millis = Math.floor(delay);
      }
      // if(isDL()) dl.log(`[0] millis = ${millis}.`);
    } else if (typeof delay === 'string') {   // How can this happen??? But, it does happen due to strange angular/typescript behavor....
      // Note: the string value can be something like "2000" or "[20, 200]".
      // We only handle the first example here (a single number),
      //    and ignore the second example. It'll just be parsed as 0.
      try {
        let d = parseInt(delay);
        if (d > 0) {
          millis = Math.floor(d);
        }
      } catch (ex) {
        // ignore
      }
      // if(isDL()) dl.log(`[1] millis = ${millis}.`);
    } else {
      if (delay) {
        let len = delay.length;
        if (len == 1) {
          if (delay[0] > 0) {
            millis = Math.floor(delay[0]);
          }
          // if(isDL()) dl.log(`[2] millis = ${millis}.`);
        } else if (len == 2) {  // Interval: [min, max)
          let min = (delay[0] < 0) ? 0 : Math.floor(delay[0]);
          let max = (delay[1] < min) ? min : Math.floor(delay[1]);
          if (min == max) {
            millis = min;
          } else {
            millis = Math.floor(Math.random() * (max - min) + min);
          }
          // if(isDL()) dl.log(`[3] millis = ${millis}.`);
        } else {
          // Invalid. Ignore.
          // if(isDL()) dl.log(`Invalid array: millis = ${millis}.`);
        }
      } else {
        // ???
        // if(isDL()) dl.log(`Invalid input: millis = ${millis}.`);
      }
    }
    // if(isDL()) dl.log(`>> Input delay = ${delay}; Output millis = ${millis}.`);
    return millis;
  }

  // temporary
  private static DEFAULT_RETRY_INTERVAL = 500;  // in millis
  private parseRetryInterval(retryInterval: (number | number[])): number {
    let interval = this.parseDelayMillis(retryInterval);
    if(interval == 0) {
      interval = LazyLoaderService.DEFAULT_RETRY_INTERVAL;
    }
    return interval;
  }

  /**
   * Load text asynchonously.
   *
   * @param url URL of the text resource.
   * @param useCache If true, the cached value may be returned.
   * @param retries If non-zero, the http get requests are retried up to the specified number.
   * @param retryInterval Interval between retries in milliseconds.
   */
  public loadText(
    url: string,
    useCache: boolean = false,
    retries: number = 0,
    retryInterval: (number | number[]) = 0
  ): Observable<string> {
    // if(! UrlUtil.isAbsolute(url)) {
    //   url = this.origin + url;      // For SSR (Angular Universal).
    // }

    // tbd:
    if (useCache && url in this.textCache) {
      // return Observable.of(this.textCache[url]).share();
      return Observable.create(o => {
        let c = this.textCache[url];
        o.next(c);
      }).share();
    } else {
      let geturl = UrlUtil.isAbsolute(url) ? url : this.origin + url;
      let obs = this.http.get(geturl, { responseType: 'text' });
      if (retries > 0) {
        let interval = this.parseRetryInterval(retryInterval);
        obs = obs.pipe(retryWhen(errors => {
          return errors.pipe(delay(interval),take(retries),
            concat(observableThrowError(new Error(`Http get request failed after ${retries} retries.`))),);
        }));
      }
      obs = obs.pipe(share());
      if (useCache) {
        obs.subscribe(content => {
          if (content != null) {
            this.textCache[url] = content;
          }
        });
      }
      return obs;
    }
  }

  /**
   * Load text asynchonously, and delayed.
   *
   * @param url URL of the text resource.
   * @param delayTime Delay time in millis, or specified as a range.
   * @param useCache If true, the cached value may be returned.
   * @param retries If non-zero, the http get requests are retried up to the specified number.
   * @param retryInterval Interval between retries in milliseconds.
   */
  public loadTextDelayed(
    url: string,
    delayTime: (number | number[]),
    useCache: boolean = false,
    retries: number = 0,
    retryInterval: (number | number[]) = 0
  ): Observable<string> {
    // if(! UrlUtil.isAbsolute(url)) {
    //   url = this.origin + url;      // For SSR (Angular Universal).
    // }

    // tbd:
    // Version a: Delay even if cache is available...
    // let millis = this.parseDelayMillis(delay);
    // if(millis == 0) {
    //   return this.loadText(url, useCache);
    // } else {
    //   return Observable.timer(millis).flatMap(i => {
    //     return this.loadText(url, useCache);
    //   });
    // }

    // Version b: Delay only if cache is not available...
    if (useCache && url in this.textCache) {
      // return Observable.of(this.textCache[url]).share();
      return Observable.create(o => {
        let c = this.textCache[url];
        o.next(c);
      }).share();
    } else {
      // let millis = this.parseDelayMillis(delay);
      // Observable.timer(millis).subscribe(i => {
      //   let obs = this.http.get(url, { responseType: 'text' }).share();
      //   if (useCache) {
      //     obs.subscribe(content => {
      //       this.textCache[url] = content;
      //     });
      //   }
      //   return obs;
      // });

      // return Observable.create(o => {
      //   let millis = this.parseDelayMillis(delay);
      //   let obs1 = Observable.timer(millis).map(i => i.toString());
      //   let x = obs1.map(i => {
      //     this.http.get(url, { responseType: 'text' }).subscribe(content => {
      //       if (useCache) {
      //         this.textCache[url] = content;
      //       }
      //       return content;
      //     })
      //   });
      //   obs1.subscribe(c => {
      //     o.next(c);
      //   });
      //   let obs2 = this.http.get(url, { responseType: 'text' });
      //   let X = Observable.concat(obs1, obs2);
      // }).share();

      // let millis = this.parseDelayMillis(delay);
      // let obs1 = Observable.timer(millis).map(i => i.toString());
      // let obs2 = this.http.get(url, { responseType: 'text' });
      // let X = Observable.concat(obs1, obs2).share();
      // if (useCache) {
      //   X.subscribe(content => {
      //     this.textCache[url] = content;
      //   });
      // }
      // return X;

      let millis = this.parseDelayMillis(delayTime);
      let obs = observableTimer(millis).pipe(   // .take(1) ???
        mergeMap(i => {
          let geturl = UrlUtil.isAbsolute(url) ? url : this.origin + url;
          let h = this.http.get(geturl, { responseType: 'text' });
          if (retries > 0) {
            let interval = this.parseRetryInterval(retryInterval);
            h = h.pipe(retryWhen(errors => {
              return errors.pipe(delay(interval),take(retries),
                concat(observableThrowError(new Error(`Http get request failed after ${retries} retries.`))),);
            }));
          }
          return h;
        }),share(),);
      if (useCache) {
        obs.subscribe(content => {
          if (content != null) {
            this.textCache[url] = content;
          }
        });
      }
      return obs;
    }
  }

  // public loadJson(url: string, useCache: boolean = false): Observable<string> {
  //   // tbd:
  //   return this.http.get(url, { responseType: 'json' }).map(o => JSON.stringify(o)).share();
  // }
  /**
   * Load remote object asynchonously.
   *
   * @param url URL of the text resource.
   * @param useCache If true, the cached value may be returned.
   * @param retries If non-zero, the http get requests are retried up to the specified number.
   * @param retryInterval Interval between retries in milliseconds.
   */
  public loadJson(
    url: string,
    useCache: boolean = false,
    retries: number = 0,
    retryInterval: (number | number[]) = 0
  ): Observable<Object> {
    // if(! UrlUtil.isAbsolute(url)) {
    //   url = this.origin + url;      // For SSR (Angular Universal).
    // }

    // tbd:
    if (useCache && url in this.jsonCache) {
      // return Observable.of(this.jsonCache[url]).share();
      return Observable.create(o => {
        let obj = this.jsonCache[url];
        o.next(obj);
      }).share();
    } else {
      let geturl = UrlUtil.isAbsolute(url) ? url : this.origin + url;
      let obs = this.http.get(geturl, { responseType: 'json' });
      if (retries > 0) {
        let interval = this.parseRetryInterval(retryInterval);
        obs = obs.pipe(retryWhen(errors => {
          return errors.pipe(delay(interval),take(retries),
            concat(observableThrowError(new Error(`Http get request failed after ${retries} retries.`))),);
        }));
      }
      obs = obs.pipe(share());
      if (useCache) {
        obs.subscribe(obj => {
          if (obj) {
            this.jsonCache[url] = obj;
          }
        });
      }
      return obs;
    }
  }
  /**
   * Load remote object asynchonously, and delayed.
   *
   * @param url URL of the text resource.
   * @param delayTime Delay time in millis, or specified as a range.
   * @param useCache If true, the cached value may be returned.
   * @param retries If non-zero, the http get requests are retried up to the specified number.
   * @param retryInterval Interval between retries in milliseconds.
   */
  public loadJsonDelayed(
    url: string,
    delayTime: (number | number[]),
    useCache: boolean = false,
    retries: number = 0,
    retryInterval: (number | number[]) = 0
  ): Observable<Object> {
    // if(! UrlUtil.isAbsolute(url)) {
    //   url = this.origin + url;      // For SSR (Angular Universal).
    // }

    // tbd:
    // Version a: Delay even if cache is available...
    // let millis = this.parseDelayMillis(delay);
    // if(millis == 0) {
    //   return this.loadJson(url, useCache);
    // } else {
    //   return Observable.timer(millis).flatMap(i => {
    //     return this.loadJson(url, useCache);
    //   });
    // }

    // Version b: Delay only if cache is not available...
    if (useCache && url in this.jsonCache) {
      // return Observable.of(this.jsonCache[url]).share();
      return Observable.create(o => {
        let c = this.jsonCache[url];
        o.next(c);
      }).share();
    } else {
      let millis = this.parseDelayMillis(delayTime);
      let obs = observableTimer(millis).pipe(mergeMap(i => {
        let geturl = UrlUtil.isAbsolute(url) ? url : this.origin + url;
        let h = this.http.get(geturl, { responseType: 'json' });
        if (retries > 0) {
          let interval = this.parseRetryInterval(retryInterval);
          h = h.pipe(retryWhen(errors => {
            return errors.pipe(delay(interval),take(retries),
              concat(observableThrowError(new Error(`Http get request failed after ${retries} retries.`))),);
          }));
        }
        return h;
      }),share(),);
      if (useCache) {
        obs.subscribe(obj => {
          if (obj) {
            this.jsonCache[url] = obj;
          }
        });
      }
      return obs;
    }
  }


  // These sync methods will only work if cache has already been populated.
  // Otherwise, they will just return null.

  public getText(url: string): (string | null) {
    if (url in this.textCache) {
      return this.textCache[url];
    } else {
      return null;
    }
  }

  public getJson(url: string): (Object | null) {
    if (url in this.jsonCache) {
      return this.jsonCache[url];
    } else {
      return null;
    }
  }


  // tbd:
  // ...

}

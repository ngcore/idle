# @ngcore/idle
> NG Core angular/typescript "idle" library


General helper llibrary for async ops, lazy loading, and delayed processing, etc.
(Current version requires Angular v7.2+)


## API Docs

* http://ngcore.gitlab.io/idle/




